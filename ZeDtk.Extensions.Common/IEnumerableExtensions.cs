﻿using System.Collections.Generic;
using System.Linq;

namespace System
{
    public static partial class IEnumerableExtensions
    {
        /// <summary>
        /// A generic version of System.String.Join()
        /// </summary>
        /// <typeparam name="T">The type of the array to join</typeparam>
        /// <param name="separator">The separator to appear between each element</param>
        /// <param name="value">An array of values</param>
        /// <returns>The join.</returns>
        /// <remarks>
        /// Contributed by Michael T, http://about.me/MichaelTran
        /// </remarks>
        public static string Join<T>(this IEnumerable<T> source, string separator)
        {
            if (source == null || !source.Any())
            {
                return string.Empty;
            }

            return string.Join(separator ?? string.Empty, source.Select(x => x.ToString()));
        }
    }
}
