﻿using System;
using Xunit;

namespace zedtk.extensions.Common.Tests
{
    public class StringExtensions_Common_Test
    {
        [Theory]
        [InlineData("NotNullOrEmpty")]
        [InlineData(" ")]
        public void IsNullOrEmpty_ReturnFalseGivenNotNullOrEmpty(string value)
        {
            bool result = value.IsNullOrEmpty();

            Assert.False(result, $"'{value}' should not be considered null or empty");
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        public void IsNullOrEmpty_ReturnTrueGivenNullOrEmpty(string value)
        {
            bool result = value.IsNullOrEmpty();

            Assert.True(result, "'{value}' should be considered null or empty");
        }
    }
}
